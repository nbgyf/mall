package com.macro.mall.ums.controller;

import com.macro.mall.dto.CommonResult;
import com.macro.mall.ums.model.UmsMemberLevel;

import com.macro.mall.ums.service.MemberLevelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * 会员等级表
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:41:04
 */
@Controller
@Api(tags = "MemberLevelController", description = "会员等级表管理")
@RequestMapping("/ums/memberLevel")
public class MemberLevelController {
    @Resource
    private MemberLevelService memberLevelService;


    @ApiOperation(value = "添加会员等级表")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberLevel:create')")
    public Object create(@Validated @RequestBody UmsMemberLevel umsMemberLevel, BindingResult result) {
        CommonResult commonResult;
        int count = memberLevelService.createMemberLevel(umsMemberLevel);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }

    @ApiOperation(value = "更新会员等级表")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberLevel:update')")
    public Object update(@PathVariable("id") Long id,
                         @Validated @RequestBody UmsMemberLevel umsMemberLevel,
                         BindingResult result) {
        CommonResult commonResult;
        int count = memberLevelService.updateMemberLevel(id, umsMemberLevel);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }

    @ApiOperation(value = "删除会员等级表")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberLevel:delete')")
    public Object delete(@PathVariable("id") Long id) {
        int count = memberLevelService.deleteMemberLevel(id);
        if (count == 1) {
            return new CommonResult().success(null);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation(value = "根据会员等级表名称分页获取会员等级表列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberLevel:read')")
    public Object getList(UmsMemberLevel memberLevel,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        return new CommonResult().pageSuccess(memberLevelService.listMemberLevel(memberLevel, pageNum, pageSize));
    }

    @ApiOperation(value = "根据编号查询会员等级表信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberLevel:read')")
    public Object getItem(@PathVariable("id") Long id) {
        return new CommonResult().success(memberLevelService.getMemberLevel(id));
    }

}
