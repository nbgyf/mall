package com.macro.mall.ums.service;

import com.macro.mall.ums.model.UmsMemberLevel;


import java.util.List;

/**
 * 会员等级表
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:41:04
 */
public interface MemberLevelService {


    int createMemberLevel(UmsMemberLevel memberLevel);
    
    int updateMemberLevel(Long id, UmsMemberLevel memberLevel);

    int deleteMemberLevel(Long id);



    List<UmsMemberLevel> listMemberLevel(UmsMemberLevel memberLevel, int pageNum, int pageSize);

    UmsMemberLevel getMemberLevel(Long id);

   
}
