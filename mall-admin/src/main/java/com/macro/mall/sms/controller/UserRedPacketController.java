package com.macro.mall.sms.controller;

import com.macro.mall.annotation.SysLog;
import com.macro.mall.dto.CommonResult;
import com.macro.mall.sms.model.SmsUserRedPacket;
import com.macro.mall.sms.service.UserRedPacketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户红包
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:37
 */
@Controller
@Api(tags = "UserRedPacketController", description = "用户红包管理")
@RequestMapping("/sms/userRedPacket")
public class UserRedPacketController {
    @Resource
    private UserRedPacketService userRedPacketService;

    @SysLog(MODULE = "sms", REMARK = "添加用户红包")
    @ApiOperation(value = "添加用户红包")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:userRedPacket:create')")
    public Object create(@Validated @RequestBody SmsUserRedPacket smsUserRedPacket, BindingResult result) {
        CommonResult commonResult;
        int count = userRedPacketService.createUserRedPacket(smsUserRedPacket);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }
    @SysLog(MODULE = "sms", REMARK = "更新用户红包")
    @ApiOperation(value = "更新用户红包")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:userRedPacket:update')")
    public Object update(@PathVariable("id") Integer id,
                         @Validated @RequestBody SmsUserRedPacket smsUserRedPacket,
                         BindingResult result) {
        CommonResult commonResult;
        int count = userRedPacketService.updateUserRedPacket(id, smsUserRedPacket);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }
    @SysLog(MODULE = "sms", REMARK = "删除用户红包")
    @ApiOperation(value = "删除用户红包")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:userRedPacket:delete')")
    public Object delete(@PathVariable("id") Integer id) {
        int count = userRedPacketService.deleteUserRedPacket(id);
        if (count == 1) {
            return new CommonResult().success(null);
        } else {
            return new CommonResult().failed();
        }
    }
    @SysLog(MODULE = "sms", REMARK = "根据用户红包分页获取用户红包列表")
    @ApiOperation(value = "根据用户红包名称分页获取用户红包列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object getList(SmsUserRedPacket userRedPacket,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        return new CommonResult().pageSuccess(userRedPacketService.listUserRedPacket(userRedPacket, pageNum, pageSize));
    }
    @SysLog(MODULE = "sms", REMARK = "根据编号查询用户红包信息")
    @ApiOperation(value = "根据编号查询用户红包信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:userRedPacket:read')")
    public Object getItem(@PathVariable("id") Integer id) {
        return new CommonResult().success(userRedPacketService.getUserRedPacket(id));
    }

}
