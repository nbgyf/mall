package com.macro.mall.sys.service;

import com.macro.mall.model.SystemOperationLog;

import java.util.List;

/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-03 17:43:34
 */
public interface WebLogService {


    int createWebLog(SystemOperationLog webLog);
    
    



    List<SystemOperationLog> listWebLog(SystemOperationLog webLog, int pageNum, int pageSize);

    SystemOperationLog getWebLog(Long id);

   
}
