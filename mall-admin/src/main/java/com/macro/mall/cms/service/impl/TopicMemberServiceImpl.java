package com.macro.mall.cms.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.cms.mapper.TopicMemberMapper;
import com.macro.mall.cms.model.CmsTopicMember;
import com.macro.mall.cms.service.TopicMemberService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品品牌Service实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class TopicMemberServiceImpl implements TopicMemberService {
    @Resource
    private TopicMemberMapper topicMemberMapper;
    


    @Override
    public int createTopicMember(CmsTopicMember topicMember) {
        return topicMemberMapper.save(topicMember);
    }

    @Override
    public int updateTopicMember(Long id, CmsTopicMember topicMember) {
        topicMember.setId(id);
        return topicMemberMapper.update(topicMember);
    }

    @Override
    public int deleteTopicMember(Long id) {
        return topicMemberMapper.remove(id);
    }


    @Override
    public List<CmsTopicMember> listTopicMember(CmsTopicMember topicMember, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return topicMemberMapper.list(topicMember);

    }

    @Override
    public CmsTopicMember getTopicMember(Long id) {
        return topicMemberMapper.get(id);
    }

   
}
