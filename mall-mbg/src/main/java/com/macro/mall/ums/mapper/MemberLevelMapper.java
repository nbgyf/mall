package com.macro.mall.ums.mapper;


import com.macro.mall.ums.model.UmsMemberLevel;
import com.macro.mall.ums.model.UmsMemberLevelExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 会员等级表
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:41:04
 */

public interface MemberLevelMapper {

	int countByExample(UmsMemberLevelExample example);

	int deleteByExample(UmsMemberLevelExample example);

	int deleteByPrimaryKey(Long id);

	int insert(UmsMemberLevel record);

	int insertSelective(UmsMemberLevel record);

	List<UmsMemberLevel> selectByExample(UmsMemberLevelExample example);

	UmsMemberLevel selectByPrimaryKey(Long id);

	int updateByExampleSelective(@Param("record") UmsMemberLevel record, @Param("example") UmsMemberLevelExample example);

	int updateByExample(@Param("record") UmsMemberLevel record, @Param("example") UmsMemberLevelExample example);

	int updateByPrimaryKeySelective(UmsMemberLevel record);

	int updateByPrimaryKey(UmsMemberLevel record);
	
	List<UmsMemberLevel> list(UmsMemberLevel memberLevel);

}
