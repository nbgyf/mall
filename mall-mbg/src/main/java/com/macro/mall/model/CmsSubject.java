package com.macro.mall.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CmsSubject implements Serializable {
    private Long id;

    private Long categoryId;

    private String title;

    /**
     * 专题主图
     *
     * @mbggenerated
     */
    private String pic;

    /**
     * 关联产品数量
     *
     * @mbggenerated
     */
    private Integer productCount;

    private Integer recommendStatus;

    private Date createTime;

    private Integer collectCount;

    private Integer readCount;

    private Integer commentCount;

    /**
     * 画册图片用逗号分割
     *
     * @mbggenerated
     */
    private String albumPics;

    private String description;

    /**
     * 显示状态：0->不显示；1->显示
     *
     * @mbggenerated
     */
    private Integer showStatus;

    /**
     * 转发数
     *
     * @mbggenerated
     */
    private Integer forwardCount;

    /**
     * 专题分类名称
     *
     * @mbggenerated
     */
    private String categoryName;

    private String content;
    private  int  is_favorite ;

    private Long areaId;
    private Long schoolId;
    private Long memberId;
    private Integer reward;


}