package com.macro.mall.model;

import com.macro.mall.dto.TreeEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-03-28 16:50:35
 */
@Data
public class Area implements TreeEntity<Area> , Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//
	private Long pid;
	//层级
	private Integer deep;
	//名称
	private String name;
	//拼音前缀
	private String pinyinPrefix;
	//拼音
	private String pinyin;
	//备注名
	private String extId;
	//备注名
	private String extName;

	public List<Area> childList;
}
