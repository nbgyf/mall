package com.macro.mall.ums.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:44:20
 */
@Data
public class UmsMemberBlanceLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//
	private Long memberId;
	//
	private Integer price;
	//
	private Integer type; // 1 消费 2 收入
	//
	private String note;

	private Date createTime;
}
