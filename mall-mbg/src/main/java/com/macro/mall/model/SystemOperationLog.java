package com.macro.mall.model;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SystemOperationLog implements Serializable {
    private Long id;

    private String keyword;
    private Long userId;

    /**
     * 用户名
     */

    private String userName;

    /**
     * 模块名
     */

    private String serviceName;

    /**
     * 记录类名+方法名
     */

    private String method;

    private String startTime;

    private String endTime;

    /**
     * 描述
     */

    private String operationDesc;

    private String createTimevar;
    private Date createTime;


    private String ip;

    private String params;

    private static final long serialVersionUID = 1L;


}