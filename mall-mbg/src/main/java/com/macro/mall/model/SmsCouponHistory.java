package com.macro.mall.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SmsCouponHistory implements Serializable {
    private Long id;

    private Long couponId;

    private Long memberId;

    private String couponCode;

    /**
     * 领取人昵称
     *
     * @mbggenerated
     */
    private String memberNickname;

    /**
     * 获取类型：0->后台赠送；1->主动获取
     *
     * @mbggenerated
     */
    private Integer getType;

    private Date createTime;

    /**
     * 使用状态：0->未使用；1->已使用；2->已过期
     *
     * @mbggenerated
     */
    private Integer useStatus;

    private Date startTime;

    private Date endTime;

    private String note;
    /**
     * 使用时间
     *
     * @mbggenerated
     */
    private Date useTime;

    /**
     * 订单编号
     *
     * @mbggenerated
     */
    private Long orderId;

    /**
     * 订单号码
     *
     * @mbggenerated
     */
    private String orderSn;

    private static final long serialVersionUID = 1L;


}