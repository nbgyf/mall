package com.macro.mall.cms.mapper;

import com.macro.mall.cms.model.CmsTopicMember;

import java.util.List;


/**
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-04 14:04:34
 */

public interface TopicMemberMapper {

	CmsTopicMember get(Long id);
	
	List<CmsTopicMember> list(CmsTopicMember topicMember);

    int count(CmsTopicMember topicMember);
	
	int save(CmsTopicMember topicMember);
	
	int update(CmsTopicMember topicMember);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
