package com.macro.mall.sms.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



/**
 * 红包
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:35
 */
@Data
public class SmsRedPacket implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//红包编号
	private Integer id;
	//发红包的用户id
	private Long userId;
	//红包金额
	private BigDecimal amount;
	//发红包日期
	private Date sendDate;
	//红包总数
	private Integer total;
	//单个红包的金额
	private BigDecimal unitAmount;
	//红包剩余个数
	private Integer stock;
	//红包类型
	private Integer type;// 1随机红包 2等额红包
	//备注
	private String note;

	private Integer status; // 1 已领取 2 未领取

	private BigDecimal reciveAmount;
}
