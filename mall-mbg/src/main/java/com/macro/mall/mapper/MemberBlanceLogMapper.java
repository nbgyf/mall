package com.macro.mall.mapper;


import com.macro.mall.ums.model.UmsMemberBlanceLog;

import java.util.List;


/**
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:44:20
 */

public interface MemberBlanceLogMapper {

	UmsMemberBlanceLog get(Long id);
	
	List<UmsMemberBlanceLog> list(UmsMemberBlanceLog memberBlanceLog);
	
	int save(UmsMemberBlanceLog memberBlanceLog);
	
	int update(UmsMemberBlanceLog memberBlanceLog);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
