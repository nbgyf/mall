package com.macro.mall.mapper;

import com.macro.mall.ums.model.TSchool;

import java.util.List;


/**
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:43:58
 */

public interface SchoolMapper {

	TSchool get(Long id);
	
	List<TSchool> list(TSchool school);
	
	int save(TSchool school);
	
	int update(TSchool school);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);
}
