package com.macro.mall.portal.vo;


import com.macro.mall.model.CmsSubject;
import com.macro.mall.model.PmsProductAttributeCategory;
import com.macro.mall.model.SmsCoupon;
import com.macro.mall.model.SmsHomeAdvertise;
import com.macro.mall.sms.model.SmsRedPacket;
import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2017/10/18 0018.
 */
@Data
public class IndexData {
    private List<TArticleDO> module_list;
    private List<SmsHomeAdvertise> banner_list;
    private List<TArticleDO> nav_icon_list;
    private List<PmsProductAttributeCategory> cat_list;
    private int cat_goods_cols;
    private List<TArticleDO> block_list;
    private List<SmsCoupon> coupon_list;
    private List<CmsSubject> subjectList;

    private List<SmsRedPacket> redPacketList;


}
