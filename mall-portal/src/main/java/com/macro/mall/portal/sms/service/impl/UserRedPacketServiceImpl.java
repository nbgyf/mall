package com.macro.mall.portal.sms.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.sms.mapper.UserRedPacketMapper;
import com.macro.mall.sms.model.SmsUserRedPacket;
import com.macro.mall.sms.service.UserRedPacketService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品品牌Service实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class UserRedPacketServiceImpl implements UserRedPacketService {
    @Resource
    private UserRedPacketMapper userRedPacketMapper;
    


    @Override
    public int createUserRedPacket(SmsUserRedPacket userRedPacket) {
        return userRedPacketMapper.save(userRedPacket);
    }

    @Override
    public int updateUserRedPacket(Integer id, SmsUserRedPacket userRedPacket) {
        userRedPacket.setId(id);
        return userRedPacketMapper.update(userRedPacket);
    }

    @Override
    public int deleteUserRedPacket(Integer id) {
        return userRedPacketMapper.remove(id);
    }


    @Override
    public List<SmsUserRedPacket> listUserRedPacket(SmsUserRedPacket userRedPacket, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return userRedPacketMapper.list(userRedPacket);

    }

    @Override
    public SmsUserRedPacket getUserRedPacket(Integer id) {
        return userRedPacketMapper.get(id);
    }

   
}
