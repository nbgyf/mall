package com.macro.mall.portal.pay.controller.alipay;

import com.jpay.alipay.AliPayApiConfig;

public abstract class AliPayApiController{
	public abstract  AliPayApiConfig getApiConfig();
}