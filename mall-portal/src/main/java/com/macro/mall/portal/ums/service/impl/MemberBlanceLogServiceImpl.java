package com.macro.mall.portal.ums.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.MemberBlanceLogMapper;
import com.macro.mall.portal.ums.service.MemberBlanceLogService;
import com.macro.mall.ums.model.UmsMemberBlanceLog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品品牌Service实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class MemberBlanceLogServiceImpl implements MemberBlanceLogService {
    @Resource
    private MemberBlanceLogMapper memberBlanceLogMapper;
    


    @Override
    public int createMemberBlanceLog(UmsMemberBlanceLog memberBlanceLog) {
        return memberBlanceLogMapper.save(memberBlanceLog);
    }

    @Override
    public int updateMemberBlanceLog(Long id, UmsMemberBlanceLog memberBlanceLog) {
        memberBlanceLog.setId(id);
        return memberBlanceLogMapper.update(memberBlanceLog);
    }

    @Override
    public int deleteMemberBlanceLog(Long id) {
        return memberBlanceLogMapper.remove(id);
    }


    @Override
    public List<UmsMemberBlanceLog> listMemberBlanceLog(UmsMemberBlanceLog memberBlanceLog, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return memberBlanceLogMapper.list(memberBlanceLog);

    }

    @Override
    public UmsMemberBlanceLog getMemberBlanceLog(Long id) {
        return memberBlanceLogMapper.get(id);
    }

   
}
